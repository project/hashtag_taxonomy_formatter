README.txt
==========

This is a small module written to provide a hashtag field formatter for 
taxonomy items. You may use it in Views or any other place the field formatter 
applies.

The module tries to convert the term name to a social-safe-hashtag format.

There are options to keep or remove non Latin characters and/or transliterate 
them, by using the transliteration module.

AUTHOR/MAINTAINER
======================
Author: Thanos Nokas(Matrixlord)
Maintainer:  Thanos Nokas(Matrixlord) (https://drupal.org/user/1538394)
